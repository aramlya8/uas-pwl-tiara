from django.contrib import admin
from .models import Pengarang, Genre, Buku, DetailPublikasi

@admin.register(Pengarang)
class PengarangAdmin(admin.ModelAdmin):
    list_display = ('nama', 'biografi')

@admin.register(Genre)
class GenreAdmin(admin.ModelAdmin):
    list_display = ('nama',)

@admin.register(Buku)
class BukuAdmin(admin.ModelAdmin):
    list_display = ('judul', 'pengarang', 'display_genre', 'isbn')
    list_filter = ('pengarang', 'genre')

    def display_genre(self, obj):
        """Membuat string untuk Genre. Diperlukan karena Genre adalah ManyToManyField."""
        return ', '.join(genre.nama for genre in obj.genre.all()[:3])
    
    display_genre.short_description = 'Genre'

@admin.register(DetailPublikasi)
class DetailPublikasiAdmin(admin.ModelAdmin):
    list_display = ('buku', 'tahun_terbit', 'edisi')
