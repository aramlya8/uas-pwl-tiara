from django.db import models

class Pengarang(models.Model):
    nama = models.CharField(max_length=100)
    biografi = models.TextField()

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = "1. Pengarang"


class Genre(models.Model):
    nama = models.CharField(max_length=100)

    def __str__(self):
        return self.nama
    
    class Meta:
        verbose_name_plural = "2. Genre"


class Buku(models.Model):
    judul = models.CharField(max_length=200)
    pengarang = models.ForeignKey(Pengarang, on_delete=models.CASCADE)
    genre = models.ManyToManyField(Genre)
    # Asumsikan ISBN unik untuk setiap buku
    isbn = models.CharField(max_length=13, unique=True)

    def __str__(self):
        return self.judul

    class Meta:
        verbose_name_plural = "3. Buku"


class DetailPublikasi(models.Model):
    buku = models.OneToOneField(Buku, on_delete=models.CASCADE, primary_key=True)
    tahun_terbit = models.IntegerField()
    edisi = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.buku.judul} - Edisi {self.edisi}"

    class Meta:
        verbose_name_plural = "4. DetailPublikasi"
