from rest_framework import status # type: ignore
from rest_framework.decorators import api_view  # type: ignore
from rest_framework.response import Response # type: ignore
from django.contrib.auth.models import User

from berita.serializers import BiodataSerializers, KategoriSerializer, ArtikelSerializers
from berita.models import Kategori, Artikel
from pengguna.models import Biodata

@api_view(['GET'])
def api_author_list(request):
    biodata = Biodata.objects.all()
    serializer = BiodataSerializers(biodata, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def api_kategori_list(request):
    kategori = Kategori.objects.all()
    serializer = KategoriSerializer(kategori, many=True)
    return Response(serializer.data)


@api_view(['GET', 'PUT'])
def api_kategori_detail(request, id_kategori):
    try:
       kategori = Kategori.objects.get(id=id_kategori)
    except:
       return Response({'message: Data Kategori Tidak Ditemukan'}, status=status.HTTP_404_NOT_FOUND)    
    
    if request.method == "GET":
       serializer = KategoriSerializer(kategori, many=False)
       return Response(serializer.data)
    
    elif request.method == "PUT":
       serializer = KategoriSerializer(kategori, data=request.data)
       if serializer.is_valid():
           serializer.save()
           return Response(serializer.data)
       
       return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    else:pass
   

@api_view(['POST'])
def api_kategori_add(request):
    serializer = KategoriSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
def api_artikel_list(request):
    key_token = "45cb25b34e021b2b2da30c6bf54e2e2d9dbab391"
    
    token = request.headers.get('token')
    if token == None:
        return Response({'message: Masukkan Token Terlebih Dahulu'}, status=status.HTTP_401_UNAUTHORIZED)
    
    if token != key_token:
    # token = Token.objects.filter(token=token)
        return Response({'message: Masukkan Token Yang Benar'}, status=status.HTTP_401_UNAUTHORIZED)

    artikel = Artikel.objects.all()
    serializer = ArtikelSerializers(artikel, many=True)
    data = {
        'count': artikel.count(),
        'rows': serializer.data
    }
    return Response(data)


@api_view(['GET', 'PUT', 'DELETE'])
def api_artikel_detail(request, id_artikel):
    try:
       artikel = Artikel.objects.get(id=id_artikel)
    except:
        return Response({'message: Data Artikel Tidak Ditemukan'}, status=status.HTTP_404_NOT_FOUND) 
    
    if request.method == "GET":
       serializer = ArtikelSerializers(artikel, many=False)
       return Response(serializer.data)
    
    elif request.method == "PUT":
        serializer = ArtikelSerializers(artikel, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    elif request.method == "DELETE":
        artikel.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def api_artikel_add(request):
    print("nurade")
    serializer = ArtikelSerializers(data=request.data)
    if serializer.is_valid():
        serializer.savee()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)