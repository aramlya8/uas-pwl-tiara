import datetime
from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from ckeditor_uploader.fields import RichTextUploadingField

class Kategori(models.Model):
    nama = models.CharField(max_length=100)

    def __str__(self):
        return self.nama

    class Meta:
        verbose_name_plural = "1. Kategori"

class Artikel(models.Model):
    judul = models.CharField(max_length=255)
    isi = RichTextUploadingField(
        config_name='special',
        external_plugin_resources=[(
            'youtube',
            'https://tiara.kelompok1-pwl-kelasa.my.id/static/ckeditor_plugins/youtube/youtube/',
            'plugin.js',
        )],
        blank=True,
        null=True
    )
    kategori = models.ForeignKey(Kategori, on_delete=models.SET_NULL, blank=True, null=True)
    author = models.ForeignKey(User, on_delete=models.PROTECT)
    thumbnail = models.ImageField(upload_to='artikel', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    slug = models.SlugField(max_length=255, unique=True, blank=True, null=True)

    def __str__(self):
        return self.judul
    
    def save(self, *args, **kwargs):
        if not self.slug:
            # Ambil waktu saat ini setiap kali objek disimpan
            x = datetime.datetime.now()
            # Buat slug dari judul artikel
            base_slug = slugify(self.judul)
            self.slug = base_slug
            counter = 1
            # Cek keunikan slug, tambahkan angka unik jika perlu
            while Artikel.objects.filter(slug=self.slug).exists():
                self.slug = f"{base_slug}-{counter}"
                counter += 1
        
        super(Artikel, self).save(*args, **kwargs)
    
    class Meta:
        verbose_name_plural = "2. Artikel"
